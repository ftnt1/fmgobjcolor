# README #

FortiManager Object Color Updater

## Files ##

### fmg_update_colors.py ###

This script will log in to FortiManager with API user and update address, addrgrp and vip object colors according to it's configuration (script needs to be edited manually)

### README.md ###

This file

## fmgpass ###

Credentials file: first line is FMG IP address, second is API user username, third is password, fourth is ADOM

### ftntlib-0.4.0.dev13.tgz ###

Fortinet library necessary to use the script. More details inside package

## Usage ##

Run it with credentials file properly filled in or with command line arguments (available after running "fmg_update_colors.py -h"

### Example: ###

fmg_update_colors.py

::: connecting to 10.109.19.70...
::: mass updating address objects
::: get_url: /pm/config/adom/root/obj/firewall/address
:::   time taken: 132 msec
:::
::: updating _dmz_test to color 23
::: updating _ext_test to color 6
::: updating _int_test to color 18
::: Updating 3 objects.
::: set_url: /pm/config/adom/root/obj/firewall/address
:::   time taken: 138 msec
:::
::: end.
::: mass updating addrgrp objects
::: get_url: /pm/config/adom/root/obj/firewall/addrgrp
:::   time taken: 118 msec
:::
::: updating _lan_test to color 18
::: Updating 1 objects.
::: set_url: /pm/config/adom/root/obj/firewall/addrgrp
:::   time taken: 125 msec
:::
::: end.
::: mass updating vip objects
::: get_url: /pm/config/adom/root/obj/firewall/vip
:::   time taken: 129 msec
:::
::: updating eeee to color 9
::: Updating 1 objects.
::: set_url: /pm/config/adom/root/obj/firewall/vip
:::   time taken: 147 msec
:::
::: end.

Finished! Press any key to exit.....
