#!/usr/bin/env python
#   about: update object colors in bulk
#  author: David Sobon <david.sobon@glencore.com>
#    date: 07 Dec 2017
# updated: 17 May 2018

__author__ = "David Sobon"
__email__ = "david.sobon@glencore.com"
__credits__ = "Lukasz Korbasiewicz"
__status__ = "Development"
__version__ = "1.2"


import time
import sys
import re
import os
import hashlib
import json
import base64
import argparse
from ftntlib import FortiManagerJSON
from netaddr import IPNetwork, IPAddress

#
# read credentials.
# check if file exists.

def filelist():
    return os.listdir(os.path.dirname(os.path.realpath(__file__)))

def credfilecheck():
    credexists = None
    for file in filelist():
        if file == 'fmgpass':
            credexists = 1
    return credexists


def commandline_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ip', action="store", help='FortiManager IP address', default=None)
    parser.add_argument('-l', '--login', action="store", help='FortiManager API login', default=None)
    parser.add_argument('-p', '--password', action="store", help='FortiManager API password', default=None)
    parser.add_argument('-a', '--adom', action="store", help='ADOM which you want to update, default is root', default='root')
    args = parser.parse_args()
    # checking if all required command line arguments are provided together
    if len([x for x in (args.ip,args.login,args.password) if x is not None]) == 1:
       parser.error('Arguments --ip, --login and --password must be given together, otherwise you can use "fmgpass" credentials file')
    if args.ip is not None:
        return(args.ip,args.login,args.password)
    else:
        return

def credentials_file():
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "fmgpass"), 'r') as f:
        ip       = f.readline().strip()
        login    = f.readline().strip()
        password = f.readline().strip()
        adom = f.readline().strip()
    if not ip or not login or not password or not adom:
        input("\n::: error: could not read ip/login/password/adom from .fmgpass file.\nPress enter to exit...")
        sys.exit(1)
    return(ip,login,password,adom)
        
        

if commandline_args():
    ip, login, password, adom = commandline_args()
elif credfilecheck():
     ip, login, password, adom = credentials_file()
else:
    input('Credentials file "fmgpass" does not exist, please create it or use command line arguments.\nFor more details run "fmg_update_colors.py -h"\nPress enter to exit...')
    sys.exit()
#
# login
#
api = FortiManagerJSON()
print("\n::: connecting to " + ip + "...")
api.login(ip, login, password)
api.verbose('on')
api.debug('off')

#
# fmg_get_url(url)
#
def fmg_get_url(url, data=None):
    print("::: get_url: " + url)
    time_start = time.time()

    if data:
        response = api.get(url, data)
    else:
        response = api.get(url)

    time_end   = time.time()
    time_delta = (time_end - time_start)
    print(":::   time taken: %d msec" % (time_delta * 1000))
    print(":::")

    return response

#
# fmg_get_url(url)
#
def fmg_set_url(url, data):
    print("::: set_url: " + url)
    time_start = time.time()
    response   = api.set(url, data)
    time_end   = time.time()
    time_delta = (time_end - time_start)
    print(":::   time taken: %d msec" % (time_delta * 1000))
    print(":::")

    return response

#
# fmg_update_colors()
#
def fmg_update_colours(objtype):
    # error: objtype
    if objtype not in ['address','addrgrp','vip']:
        print("::: error: objtype %s is invalid." % (objtype))
        sys.exit(1)

    # go.
    print("::: mass updating %s objects" % (objtype))

    data = {
        'get flags': 1,
        'fields' : ["name", "color"]
    }
    all_data = []
    url = "/pm/config/adom/" + adom + "/obj/firewall/" + objtype
    response = fmg_get_url(url, data)
    for object in response[1]:
        # skip if global object.
        if 'obj flags' in object:
            continue

        _update = 0

        # updating all vip objects to color 9 (orange)
        if 9 != object['color'] and objtype == 'vip':
            _update = 1
            color   = 9

        # updating all objects with color 28 to 23
        if 28 == object['color']:
            _update = 1
            color   = 23

        # updating all objects with _ext_ in name to 6 (red)
        if '_ext_' in object['name']:
            if 6 != object['color']:
                _update = 1
                color   = 6

        # updating all objects with _int_ in name to 18 (green)
        if '_int_' in object['name'] or '_lan_' in object['name']:
            if 18 != object['color']:
                _update = 1
                color   = 18

        # updating all objects with _dmz_ in name to 23 (pink)
        if '_dmz_' in object['name']:
            if 23 != object['color']:
                _update = 1
                color   = 23

        # no update.
        if not _update:
            continue

        # prepare update!
        print("::: updating " + object['name'] + " to color " + str(color))

        _data = {
            'name': object['name'],
            'color': color
        }
        all_data.append(_data)

        #
        # update in bulk.
        #
        if len(all_data) == 100:
            print("::: Updating %d objects." % (len(all_data)))
            fmg_set_url(url, all_data)
            all_data = []

    # remaining objects.
    if len(all_data):
        print("::: Updating %d objects." % (len(all_data)))
        fmg_set_url(url, all_data)
        all_data = []

    print("::: end.")

#
# go.
#
# Update firewall addresses
fmg_update_colours("address")
# Update firewall address groups
fmg_update_colours("addrgrp")
# Update firewall vip
fmg_update_colours("vip")

#
# end.
#
api.debug('off')
api.logout()
input('\nFinished! Press enter to exit.....')
sys.exit(0)
